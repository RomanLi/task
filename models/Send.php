<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "send".
 *
 * @property integer $id
 * @property string $recipient
 * @property string $subject
 * @property string $body
 * @property integer $status
 */
class Send extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'send';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipient', 'subject'], 'required'],
            [['body'], 'string'],
            [['status'], 'integer'],
            [['recipient', 'subject'], 'string', 'max' => 255],
            ['status', 'default', 'value' => 0],
            ['recipient','email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipient' => 'Recipient',
            'subject' => 'Subject',
            'body' => 'Body',
            'status' => 'Status',
        ];
    }
    static function changeStatus($id)
    {
        $command = \Yii::$app->db->createCommand();
        $command->update('send', array(
            'status'=>'1',
        ), 'id=:id', array(':id'=>$id))->execute();

    }
}
