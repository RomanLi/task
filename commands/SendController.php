<?php

namespace app\commands;

use app\models\Send;
use yii\console\Controller;


class SendController extends Controller
{
    public function actionIndex()
    {
        $array = Send::find()->where(["status" => 0])->all();
        foreach ($array as $object) {
            $result = \Yii::$app->mailer->compose()
                ->setTo($object->recipient)
                ->setSubject($object->subject)
                ->setTextBody($object->body)
                ->send();
            if ($result) {
                Send::changeStatus($object->id);
            } else {
                echo "false\n";
            }
        }
    }
}
