<?php
require_once 'Mandrill.php';
$mandrill = new Mandrill('SECRET_API_KEY');

try{

    $message = array(
        'subject' => 'Subject of Email',
        'text' => 'Contents of email goes here', // or just use 'html' to support HTMl markup
        'from_email' => 'sender@example.com',
        'from_name' => 'Sender Name', //optional
        'to' => array(
            array( // add more sub-arrays for additional recipients
                'email' => 'recipient@example.com',
                'name' => 'Recipient Name', // optional
                'type' => 'to' //optional. Default is 'to'. Other options: cc & bcc
            )
        ),

        /* Other API parameters (e.g., 'preserve_recipients => FALSE', 'track_opens => TRUE',
          'track_clicks' => TRUE) go here */
    );

    $result = $mandrill->messages->send($message);
    print_r($result); //only for debugging

} catch(Mandrill_Error $e) {

    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();

    throw $e;
}