<?php

use yii\db\Migration;

class m160620_173356_send extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%send}}', [
            'id' => $this->primaryKey(),
            'recipient' => $this->string()->notNull(),
            'subject' => $this->string()->notNull(),
            'body' => $this->text(),
            'status'=>$this->smallInteger()->defaultValue(0),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%send}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
